// match pattern for the URLs to redirect
let searchTerms = "";
let pattern = "https://www.bing.com/*";

let currentEngine = "duckduckgo";
if (chrome.storage.sync.get(['engine'], (e) => {
    return e.length;
})) {
    chrome.storage.sync.get(['engine'], (e) => {
        currentEngine = e.engine;
    });
}
let engines = {
    duckduckgo: {
        redirectUrl: 'https://duckduckgo.com',
        queryParameter: '/?q='
    },
    google: {
        redirectUrl: 'https://google.com',
        queryParameter: '/search?q='
    },
    qwant: {
        redirectUrl: 'https://qwant.com',
        queryParameter: '/?q='
    },
    ask: {
        redirectUrl: 'https://ask.com',
        queryParameter: '/web?q='
    },
    baidu: {
        redirectUrl: 'https://baidu.com',
        queryParameter: '/s?wd='
    },
    yandex: {
        redirectUrl: 'https://yandex.com',
        queryParameter: '/search/?text='
    },
    yahoo: {
        redirectUrl: 'https://search.yahoo.com',
        queryParameter: '/search?p='
    },
    lilo: {
        redirectUrl: 'https://search.lilo.org',
        queryParameter: '/results.php?q='
    },
    ecosia: {
        redirectUrl: 'https://www.ecosia.org',
        queryParameter: '/search?q='
    }
};

let redirectionActive = true;
chrome.browserAction.setBadgeBackgroundColor({color: "rgb(180,0,0)"});
chrome.browserAction.onClicked.addListener(() => {
    redirectionActive = !redirectionActive;
    chrome.browserAction.setBadgeText(
        {
            text: (redirectionActive ? "" : "Off")
        }
    );
    if (redirectionActive === false) {
        chrome.webRequest.onBeforeRequest.removeListener(redirectAsync)
    } else {
        createRedirectionListener();
    }
});

chrome.storage.onChanged.addListener(

    () => currentEngine = chrome.storage.sync.get(['engine'], (e) => {
        currentEngine = e.engine;
    })
); //restore options

// redirect function returns a Promise
// which is resolved with the redirect URL when a timer expires
function redirectAsync(requestDetails) {
    let regex = /q=(.*?)(&|$)/;
    console.log(currentEngine);
    let redirectUrl = engines[currentEngine].redirectUrl;
    if (requestDetails.url.match(regex)) {
        searchTerms = requestDetails.url.match(regex);
        searchTerms = searchTerms[1];
        redirectUrl = redirectUrl + engines[currentEngine].queryParameter + searchTerms;
    }
    return {redirectUrl}
}

// add the listener,
// passing the filter argument and "blocking"
function createRedirectionListener() {
    chrome.webRequest.onBeforeRequest.addListener(
        redirectAsync,
        {urls: [pattern], types: ["main_frame"]},
        ["blocking"]
    );
}

createRedirectionListener();
